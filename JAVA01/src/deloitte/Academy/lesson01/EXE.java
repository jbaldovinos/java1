package deloitte.Academy.lesson01;

import deloitte.Academy.arithmetic.Arithmetic;
import deloitte.Academy.comparisons.Comparisons;
import deloitte.Academy.logicaloperations.LogicalOperations;

public class EXE {

	public static void main(String[] args) {
		/**
		 * Se ejecutan los metodos correspondientes a las operaciones matematicas
		 * b�sicas que se encuentran dentro de la clase "arithmetic"
		 */
		Arithmetic.suma(2, 2);
		Arithmetic.resta(10, 8);
		Arithmetic.division(10, 2);
		Arithmetic.multiplicacion(2, 4);
		Arithmetic.modulus(20, 7);

		/**
		 * Se ejecutan los m�todos correspondientes a las comparaciones b�sicas (Igual,
		 * Mayor que, Menor que, Diferente)
		 */
		Comparisons.iguales(10, 2);
		Comparisons.mayor(10, 1);
		Comparisons.menor(2, 3);
		Comparisons.noiguales(10, 10);

		/**
		 * Se ejecutan los m�todos correspondientes a las operaciones logicas tales
		 * como: NAND, AND, OR, NOR.
		 */
		LogicalOperations.AND(true, true);
		LogicalOperations.OR(false, false);
		LogicalOperations.XOR(true, false);

	}

}
