package deloitte.Academy.logicaloperations;

import java.util.logging.Logger;
import java.util.logging.Level;

public class LogicalOperations {

	private static final Logger LOGGER = Logger.getLogger(LogicalOperations.class.getName());

	/**
	 * Metodo para realizar la funci�n l�gica AND
	 * 
	 * @param a booleano
	 * @param b booleano
	 * @return : Se obtiene como retorno valor booleano correspondiente a la
	 *         operaci�n l�gica
	 */
	public static boolean AND(boolean parametroa, boolean parametrob) {
		boolean resultado = false;
		try {
			if (parametroa && parametrob) {
				LOGGER.info("Prueba satisfactoria");
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Cannot solve operation!");

		}
		return resultado;
	}

	/**
	 * Metodo OR
	 * 
	 * @param a Booleano
	 * @param b Booleano
	 * @return : Se obtiene como retorno valor booleano correspondiente a la
	 *         operaci�n l�gica OR
	 */
	public static boolean OR(boolean parametroa, boolean parametrob) {
		boolean resultado = false;
		try {
			if (parametroa || parametrob) {
				LOGGER.info("Prueba satisfactoria");
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Cannot solve operation!");

		}
		return resultado;
	}

	/**
	 * Metodo para XOR
	 * 
	 * @param a Booleano
	 * @param b Booleano
	 * @return : Se obtiene como retorno valor booleano correspondiente a la
	 *         operaci�n l�gica XOR
	 */
	public static boolean XOR(boolean parametroa, boolean parametrob) {
		boolean resultado = false;
		try {
			if (parametroa ^ parametrob) {
				LOGGER.info("Prueba satisfactoria");
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Cannot solve operation!");

		}
		return resultado;
	}
}
