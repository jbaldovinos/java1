package deloitte.Academy.comparisons;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Comparisons {
	private static final Logger LOGGER = Logger.getLogger(Comparisons.class.getName());

	/**
	 * Metodo que identifica si los valores "a" y "b" son iguales
	 * 
	 * @param a :Entero
	 * @param b :Entero
	 * @return : se obtiene como leyenda en caso de ser iguales los dos numero
	 */
	public static boolean iguales(int parametroa, int parametrob) {
		boolean resultado = false;
		try {
			if (parametroa == parametrob) {
				LOGGER.info("Los n�meros son iguales");
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Cannot solve operation!");

		}
		return resultado;
	}

	/**
	 * Metodo que identifica si los valores "a" y "b" son diferentes
	 * 
	 * @parametroa :Entero
	 * @parametrob :Entero
	 * @return : Se obtiene una leyenda que confirma en caso de que los dos valores
	 *         sean diferentes
	 */
	public static boolean noiguales(int parametroa, int parametrob) {
		boolean resultado = false;
		try {
			if (parametroa != parametrob) {
				LOGGER.info("Los n�meros no son iguales");
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Cannot solve operation!");

		}
		return resultado;
	}

	/**
	 * Metodo que identifica si el valor "a" es mayor que el valor "b"
	 * 
	 * @param a :Entero
	 * @param b :Entero
	 * @return : Se obtiene leyenda en caso de que se cumpla la condici�n
	 */
	public static boolean mayor(int parametroa, int parametrob) {
		boolean resultado = false;
		try {
			if (parametroa > parametrob) {
				LOGGER.info("Valor de A es mayor que valor de B");
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Cannot solve operation!");

		}
		return resultado;
	}

	/**
	 * Metodo que identifica si el valor "a" es menor que el valor "b"
	 * 
	 * @param a : Entero
	 * @param b : Entero
	 * @return : Se obtiene leyenda en caso de que se cumpla la condici�n
	 */
	public static boolean menor(int parametroa, int parametrob) {
		boolean resultado = false;
		try {
			if (parametroa < parametrob) {
				LOGGER.info(" es menor que B");
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Cannot solve operation!");

		}
		return resultado;
	}
}