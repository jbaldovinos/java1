package deloitte.Academy.arithmetic;

import java.util.logging.Logger;
import java.util.logging.Level;

public class Arithmetic {

	private static final Logger LOGGER = Logger.getLogger(Arithmetic.class.getName());

	/**
	 * M�todo correspondiente para hacer una suma de dos factores.
	 * 
	 * @param a : Valor de tipo entero
	 * @param b : Valor de tipo entero
	 * @return : Se obtiene el resultado de la suma de los dos valores ingresados
	 *         por el usuario.
	 */
	public static int suma(int parametroa, int parametrob) {
		int c = 0;
		try {
			c = parametroa + parametrob;
			LOGGER.info("La operaci�n fue exitosa:" + c);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "ERROR");
		}
		return c;
	}

	/**
	 * M�todo correspondiente para hacer una resta de dos factores.
	 * 
	 * @param a : Valor de tipo entero
	 * @param b : Valor de tipo entero
	 * @return : Se obtiene el resultado de la resta de los dos valores ingresados
	 *         por el usuario.
	 */
	public static int resta(int parametroa, int parametrob) {
		int c = 0;
		try {
			c = parametroa - parametrob;
			LOGGER.info("La operaci�n fue exitosa:" + c);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "ERROR");
		}
		return c;
	}

	/**
	 * M�todo correspondiente para hacer una divisi�n de dos factores.
	 * 
	 * @param a : Valor de tipo entero
	 * @param b : Valor de tipo entero
	 * @return : Se obtiene el resultado de la divisi�n de los dos valores
	 *         ingresados por el usuario.
	 */
	/**
	 * M�todo correspondiente para hacer una multiplicaci�n de dos factores.
	 * 
	 * @param a : Valor de tipo entero
	 * @param b : Valor de tipo entero
	 * @return : Se obtiene el resultado de la multiplicaci�n de los dos valores
	 *         ingresados por el usuario.
	 */
	public static int multiplicacion(int parametroa, int parametrob) {
		int c = 0;
		try {
			c = parametroa * parametrob;
			LOGGER.info("La operaci�n fue exitosa:" + c);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "ERROR");
		}
		return c;
	}

	/**
	 * M�todo correspondiente para sacar el m�dulo
	 * 
	 * @param a : Valor de tipo entero
	 * @param b : Valor de tipo entero
	 * @return : Se obtiene el resultado de la m�dulo de los dos valores ingresados
	 *         por el usuario.
	 */
	public static int modulus(int parametroa, int parametrob) {
		int c = 0;
		try {
			c = parametroa % parametrob;
			LOGGER.info("La operaci�n fue exitosa:" + c);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "ERROR");
		}
		return c;
	}
}
